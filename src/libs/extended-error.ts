export const ExtendedError = function (message: string, params: Object) {
  const error = new Error(message)
  for (const key in params) {
    error[key] = params[key]
  }
  return error
}
